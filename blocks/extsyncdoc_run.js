import {SimpleBlock} from '@/BlocklyExecutor/Core/block_templates/simple_block';
import ExtException from '@/Helpers/ExtException';
import {uuid4, wait} from '@/Helpers/BaseHelper';

export default class ExtsyncdocRun extends SimpleBlock {
    async _calc_value(node, path, context, block_context) {
        block_context['prepare_counter'] = block_context.prepare_counter || 0
        block_context['__deferred'] = block_context.__deferred || {}

        try {
            const result = await context.report.service.extSyncdocSabyRead(context, block_context)
            if (result.Direction === 1) {
                await this.extsyncdocPrepareIs(context, block_context)
                await this.extsyncdocExecuteSaby(context, block_context)
            } else if (result.Direction === 2 || result.Direction === 0) {
                await this.extsyncdocPrepareSaby(context, block_context)
                await this.extsyncdocExecuteIs(context, block_context)
            }
        } catch (err) {
            throw err
        }
        return await context.report.service.extSyncdocSabyRead(context, block_context)
    }

    async extsyncdocPrepareIs(context, block_context) {
        const connectionUuid = context.report.operation.uuid
        const extsyncdocUuid = context.report.uuid
        for (;;) {
            block_context.prepare_counter += 1
            if (block_context.prepare_counter >= 3000) {
                throw new Error('Превышено количество циклов prepare для операции экспорта')
            }
            let result
            try {
                result = await context.report.service.extsyncdocPrepare(context.report.params, extsyncdocUuid, 1)
            } catch (err) {
                throw err
            }

            for (const key of Object.keys(result)) {
                block_context[key] = result[key]
            }
            const actions = block_context.requiredActions || []
            const countActions = actions.length
            if (countActions) {
                const objects = []
                this.logger.debug(`ExtSyncDoc.prepare actions: ${result.requiredActions.length}`)
                for (const command of actions) {
                    try {
                        await this[`command_${command[0].toLowerCase()}`](context, block_context, command[1], objects)
                    } catch (err) {
                        let _data = command[1]
                        _data.StatusId = 'Ошибка'
                        if (err instanceof ExtException) {
                            _data.StatusMsg = err.message
                            _data.Title = err.detail
                            _data.Data.error = err.toDict()
                        }
                        if (err instanceof Error) {
                            _data.StatusMsg = err.message
                            objects.push(_data)
                        }
                    }
                }
                if (objects.length) {
                    await context.report.service.extsyncdocWrite(
                        context.report.params,
                        connectionUuid,
                        {
                            'Uuid': extsyncdocUuid,
                        },
                        objects
                    );
                }
            }
            if (!countActions && block_context.all_objects <= block_context.count_processed + block_context.count_error) {
                if (block_context.count_error) {
                    // throw new Error('Ошибки подготовки');
                }
                break;
            }
        }
    }

    async extsyncdocPrepareSaby(context, block_context) {
        const extsyncdocUuid = context.report.uuid
        const result = await context.report.service.extsyncDocPrepareSaby(context.params, extsyncdocUuid)
        for (const [key, value] of Object.entries(result)) {
            block_context[key] = value
        }
        return result
    }

    // eslint-disable-next-line no-unused-vars
    async extsyncdocExecuteSaby(context, block_context, ext_sync_obj, objects) {
        const extsyncdocUuid = context.report.uuid
        context.report.service.extSyncdocExecuteLrs(context.param, extsyncdocUuid, 1)
        for (;;) {
            const result = await context.report.service.extSyncdocSabyRead(context, block_context)
            if (result.CountObjects > 0 && result.CountObjects === result.CountErrors + result.CountConfirmed + result.CountProcessed) {
                break
            } else {
                const status = result.Status
                if (status === 10 || status === 20 || status === 100) {
                    break
                }
            }
            await wait(3000)
        }
    }

    async extsyncdocExecuteIs(context, block_context, ext_sync_obj, breakExtsyncobjUuid) {
        const extsyncdocUuid = context.report.uuid
        const connectionUuid = context.report.operation.uuid
        for (;;) {
            block_context.prepare_counter = (block_context.prepare_counter || 0) + 1
            if (block_context.prepare_counter >= 3000) {
                throw new Error('Превышено количество циклов prepare для операции')
            }
            try {
                const result = await context.report.service.extsyncobjGetObjForExecute(
                    context.params,
                    extsyncdocUuid,
                    [
                        'Keys',
                        'SbisType'
                    ],
                    1
                );
                if (Array.isArray(result) && !result.length) {
                    await context.report.service.extSyncdocExecute(
                        context.params,
                        extsyncdocUuid,
                        2
                    )
                    await wait(1000);
                    break
                }

                const objects = [];
                for (const obj of result) {
                    if (!breakExtsyncobjUuid && obj.Uuid === breakExtsyncobjUuid) {
                        return obj
                    }

                    const countObjects = objects.length

                    await this.command_find(context, block_context, obj, objects, obj["Keys"])

                    if (objects.length === countObjects) {
                        throw new ExtException({message: 'Команда Execute не выполнена'})
                    }

                    if (objects.length > 0) {
                        await context.report.service.extsyncdocWrite(
                            context.report.params,
                            connectionUuid,
                            {
                                'Uuid': extsyncdocUuid,
                            },
                            objects
                        );
                    }
                }
            } catch (err) {
                throw new ExtException({parent: err})
            }
        }
    }

    /**
     * @param context Глобальный контекст
     * @param block_context Контекст блока
     * @param command название команды
     * @param extSyncObj объект extSyncObj
     * @param obj аргументы вызова calc_ini
     * @param endpoint название функции для расчета
     */
    async addCommandCalcIni(context, block_context, command, extSyncObj, obj, endpoint) {
        const type = extSyncObj.ClientType ? 'ClientType' : 'Type'
        const typeList = extSyncObj[type].split('.')
        let objName = obj?.object?.ini_name
        if (!objName) {
            objName = typeList[typeList.length - 1]
        }
        const iniName = `${objName}_${command}`
        return context.report.connectorIs.loadCalcIni(iniName, obj, endpoint)
    }

    /**
     * Выполнение команды get object
     */
    async command_getobject(context, block_context, extSyncObj, objects) {
        const obj = extSyncObj.Context || {}
        obj.ИдИС = extSyncObj.ClientId
        obj.ИмяСБИС = extSyncObj.SbisType
        let dataIs = extSyncObj.Data.data_is
        if (!dataIs) {
            const type = extSyncObj.ClientType ? 'Type' : 'ClientType'
            const typeList = extSyncObj[type].split('.')
            dataIs = {
                ИдИС: extSyncObj.ClientId,
                ИмяИС: `${typeList[0]}.${typeList[1]}`,
                Регламент: extSyncObj.Data.Регламент,
                ПроизвольноеНазваниеРегламента: extSyncObj.Data.ПроизвольноеНазваниеРегламента,
                ini_name: extSyncObj.Data.ini_name.replace('СинхВыгрузка_', ''),
                _print_forms: extSyncObj.Data._print_forms
            }
        }
        obj.object = dataIs
        const endpoint = extSyncObj.Data.endpoint
        const result = await this.addCommandCalcIni(context, block_context, 'read', extSyncObj, obj, endpoint)
        this.processCommandResultRead(context, block_context, extSyncObj, result[0], objects)
    }

    async command_syncdocFill(context, block_context, extSyncObj, objects) {
        if (extSyncObj.ini_name) {
            const obj = extSyncObj.Context || {}
            const dataIs = {
                ...extSyncObj.filter
            }
            dataIs.ini_data = extSyncObj.ini_name.replace('СинхВыгрузка_', '_')
            extSyncObj.Type = dataIs.ini_data
            obj.object = dataIs
            const result = await this.addCommandCalcIni(context, block_context, 'fill', extSyncObj, obj)
            this.processCommandResultRead(context, block_context, extSyncObj, result, objects);
        }
        return ''
    }

    async command_processpredefineobject(context, block_context, extSyncObj) {
        const obj = extSyncObj.Context || {}
        obj.ИдИС = extSyncObj.ClientId
        obj.ИмяСБИС = extSyncObj.Data.ini_name.replace('СинхВыгрузка_', '')
        let dataIs = extSyncObj?.Data?.data_is
        if (!dataIs) {
            dataIs = {}
            dataIs.ИдИС = extSyncObj.ClientId
        }
        obj.object = dataIs
        const [result] = await this.addCommandCalcIni(context, block_context, 'predefine', extSyncObj, obj, '')
        if (result.status === 'complete') {
            if (Array.isArray(result.data)) {
                for (const _data of result.data) {
                    context.report.service.extsyncdocWritePredefined(
                        context.params,
                        context.report.operation.uuid,
                        extSyncObj.Type,
                        _data.ClientType,
                        _data.Objects
                    )
                }
            } else {
                context.report.service.extsyncdocWritePredefined(
                    context.params,
                    context.report.operation.uuid,
                    extSyncObj.Type,
                    result.data.ClientType,
                    result.data.Objects
                )
            }
        } else if (result.status === 'error') {
            throw ExtException({
                message: `Не удалось загрузитьпредопределенные данные = ${extSyncObj.Type}`,
                action: 'command_processpredefineobject'
            })
        }
    }

    async command_find(context, block_context, ext_sync_obj, objects, item_keys) {
        let obj
        try {
            obj = ext_sync_obj["Data"]["data"];
        } catch (err) {
            throw new ExtException({
                message: 'Объект не содержит необходимый параметр'
            })
        }

        ext_sync_obj["ClientType"] = undefined
        let calcFind = false
        let result
        if (!obj.ИдИС) {
            calcFind = true
        } else {
            const keys = {
                Value1: {
                    Uid: obj["ИдИС"],
                    Type: obj["ИмяИС"]
                }
            }
            const params = {
                object: ext_sync_obj["Data"]["data"],
                key: keys
            }
            const commandResult = await this.addCommandCalcIni(context, block_context, 'find', ext_sync_obj, params, 'find_by_key_Uid')
            result = commandResult[0]
            if ((result.status === 'complete' && !result.data) || result.status === 'error') {
                ext_sync_obj.Data.data.ИдИС = undefined
                calcFind = true
            } else {
                calcFind = false
            }
        }

        const connectionUuid = context.report.operation.uuid
        if (calcFind) {
            for (const itemKey of item_keys) {
                const params = {
                    object: ext_sync_obj["Data"]["data"],
                    key: itemKey
                }
                const commandResult = await this.addCommandCalcIni(
                    context,
                    block_context,
                    'find',
                    ext_sync_obj,
                    params,
                    `find_by_key_${itemKey["Key"]}`
                )
                result = commandResult[0]
                if (result.status === 'complete' && result.data) {
                    ext_sync_obj["Data"]["data"]["ИдИС"] = result["data"]["ИдИС"]
                    ext_sync_obj["Data"]["data"]["ИмяИС"] = result["data"]["ИмяИС"]
                    ext_sync_obj["ClientType"] = result["data"]["ИмяИС"]
                    ext_sync_obj ["ClientId"] = result["data"]["ИдИС"]

                    const _filter = {
                        ConnectionId: connectionUuid,
                        Type: ext_sync_obj["Data"]["data"]["ИмяСБИС"],
                        Id: ext_sync_obj["Data"]["data"]["ИдСБИС"],
                        IdType: 1
                    }
                    const _data = {
                        ClientId: result["data"]["ИдИС"],
                        ClientType: result["data"]["ИмяИС"],
                        Status: result["data"]["ИдИС"] ? 1 : 4,
                        Status_msg: 'Сопоставлено'
                    }

                    let numberKey = 1
                    for (const ik of item_keys) {
                        let idxKey = 1
                        for (;;) {
                            if (ik[`Value${idxKey}`]) {
                                let keyValue = ik[`Value${idxKey}`]
                                if (keyValue.Uid) {
                                    keyValue = keyValue.Uid
                                }
                                _data[`ClientParam_${numberKey}_${idxKey}`] = keyValue
                                idxKey++
                            } else {
                                break
                            }
                        }
                        numberKey++
                    }

                    objects.push({
                        Uuid: ext_sync_obj["Uuid"],
                        StatusId: 'Синхронизирован'
                    })

                    await context.report.service.mappingObjFindAndUpdate(context.params, _filter, _data)

                    ext_sync_obj.StatusId = 'Синхронизирован'
                    ext_sync_obj.ClientId = result["data"]["ИдИС"]
                    ext_sync_obj.ClientType = undefined
                } else if (result.status === 'error' && result["data"].Type === 'NotFound') {
                    const _filter = {
                        ConnectionId: connectionUuid,
                        Type: ext_sync_obj["Data"]["data"]["ИмяСБИС"],
                        Id: ext_sync_obj["Data"]["data"]["ИдСБИС"],
                        IdType: 1
                    }
                    const _data = {
                        ClientId: result["data"]["ИдИС"],
                        ClientType: result["data"]["ИмяИС"],
                        Status: 5,
                        StatusId: 'Ошибка',
                        Status_msg: result["data"].message
                    }
                    await context.report.service.mappingObjFindAndUpdate(context.params, _filter, _data)

                    objects.push({
                        Uuid: ext_sync_obj["Uuid"],
                        StatusId: 'Ошибка',
                        StatusMsg: result["data"].message,
                        Data: {
                            error: {
                                action: result["data"].action,
                                message: result["data"].message,
                                code: result["data"].detail,
                                stack: result["data"].stack,
                            }
                        }
                    })
                }
            }
        }

        if (!result.data || ext_sync_obj.Subobject === false) {
            return this.command_update(context, block_context, ext_sync_obj, objects)
        }
    }

    async command_update(context, block_context, extSyncObj, objects) {
        const obj = extSyncObj.Data.data
        if (!obj) {
            throw new ExtException({message: 'Объект не содержит необходимый параметр'})
        }
        const params = {
            object: extSyncObj["Data"]["data"]
        }
        const [result] = await this.addCommandCalcIni(context, block_context, 'update', extSyncObj, params)
        await this.processCommandResultUpdate(context, block_context, extSyncObj, result, objects)
    }

    async processCommandResultUpdate(context, block_context, command, result, objects) {
        const connectionUuid = context.report.operation.uuid

        if (result.data.ИдИС) {
            const _filter = {
                ConnectionId: connectionUuid,
                Type: command["Data"]["data"]["ИмяСБИС"],
                Id: command["Data"]["data"]["ИдСБИС"],
                IdType: 1
            }
            const _data = {
                ClientId: result["data"]["ИдИС"],
                ClientType: result["data"]["ИмяИС"],
                Status: result["data"]["ИдИС"] ? 1 : 4,
                Status_msg: 'Сопоставлено'
            }
            await context.report.service.mappingObjFindAndUpdate(context.params, _filter, _data)
            objects.push({
                Uuid: command["Uuid"],
                StatusId: 'Синхронизирован',
                ClientId: result["data"]["ИдИС"],
                ClientType: result["data"]["ИмяИС"]
            })
        } else if (result.status === 'error') {
            const _filter = {
                ConnectionId: connectionUuid,
                Type: command["Data"]["data"]["ИмяСБИС"],
                Id: command["Data"]["data"]["ИдСБИС"],
                IdType: 1
            }
            const _data = {
                ClientId: result["data"]["ИдИС"],
                ClientType: result["data"]["ИмяИС"],
                Status: 5,
                StatusId: 'Ошибка',
                Status_msg: result["data"].message
            }

            let numberKey = 1
            for (const ik of command.Keys) {
                let idxKey = 1
                for (;;) {
                    if (ik[`Value${idxKey}`]) {
                        let keyValue = ik[`Value${idxKey}`]
                        if (keyValue.Uid) {
                            keyValue = keyValue.Uid
                        }
                        _data[`ClientParam_${numberKey}_${idxKey}`] = keyValue
                        idxKey++
                    } else {
                        break
                    }
                }
                numberKey++
            }
            await context.report.service.mappingObjFindAndUpdate(context.params, _filter, _data)
            objects.push({
                Uuid: command["Uuid"],
                StatusId: 'Ошибка',
                StatusMsg: result["data"].message,
                Data: {
                    error: {
                        action: result["data"].action,
                        message: result["data"].message,
                        code: result["data"].detail,
                        stack: result["data"].stack,
                    }
                }
            })
        }
    }

    /**
     * Обрабочик ответа calc_ini (read)
     */
    processCommandResultRead(context, block_context, extSyncObj, result, objects) {
        if (result.status === 'complete') {
            const resultData = result.data
            if (Array.isArray(resultData) && resultData.length > 0) {
                this.processCommandResultReadArray(context, block_context, extSyncObj, resultData, objects)
            } else {
                this.processCommandResultReadObject(context, block_context, extSyncObj, resultData, objects)
            }
        } else if (result.status === 'error') {
            const error = result.data
            const _data = extSyncObj
            _data.Title = error.detail
            _data.StatusId = 'Ошибка'
            _data.StatusMsg = error.message
            _data.Data = {error}
            objects.push(_data)
        }
    }

    processCommandResultReadArray(context, block_context, extSyncObj, result = [], objects) {
        for (const [idx, resultItem] of result.entries()) {
            // в противном случае перетерается uuid у текущего объекта
            const cloneExtSyncObj = {...extSyncObj}
            if (idx > 0) {
                cloneExtSyncObj.Uuid = uuid4()
            }
            this.processCommandResultReadObject(context, block_context, cloneExtSyncObj, resultItem, objects)
        }
    }

    processCommandResultReadObject(context, block_context, extSyncObj, result, objects) {
        extSyncObj.Data = {
            ...extSyncObj.Data,
            data_is: result,
            name: result.Название,
            ini_name: ''
        }
        delete extSyncObj['@ExtSyncObj']
        extSyncObj.ClientId = result.ИдИС
        extSyncObj.Id = result.ИдИС
        extSyncObj.SbisType = result.ИмяСБИС
        extSyncObj.StatusId = 'Получен'
        extSyncObj.Title = result.Название
        objects.push(extSyncObj)
    }
}
