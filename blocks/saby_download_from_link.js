import {SimpleBlock} from '@/BlocklyExecutor/Core/block_templates/simple_block';

export default class SabyDownloadFromLink extends SimpleBlock {
    async _calc_value(node, path, context, block_context) {
        const downloadFromLinkResponse = await context.report.service.downloadFromLink(
            context.report.params,
            block_context.link
        )
        return atob(downloadFromLinkResponse)
    }
}
