import {SimpleBlock} from "@/BlocklyExecutor/Core/block_templates/simple_block";

export default class ExtsyncdocWrite extends SimpleBlock {
    async _calc_value(node, path, context, block_context) {
        let direction
        let objects
        try {
            direction = block_context['direction']
            objects = block_context['objects']
        } catch (err) {
            throw new Error(`${this.constructor.name} ${this.block_id} params not defined`)
        }
        if (direction === 'import') {
            direction = 1;
            objects = await this.prepare_ext_sync_objects(context, direction, objects);
        } else if (direction === 'export') {
            direction = 2;
            objects = await this.prepare_ext_sync_objects(context, direction, objects);
        } else if (direction === 'import_api3_obj') {
            direction = 1;
            objects = await this.prepare_api3_objects(context, direction, objects);
        } else if (direction === 'import_api3_link') {
            direction = 1;
            objects = await this.prepare_api3_link(context, direction, objects);
        } else {
            throw new Error('Хрень какая то');
        }

        const extsyncdocUuid = context.report.uuid;
        const connectionUuid = context.report.operation.uuid;

        try {
            await context.report.service.extsyncdocWrite(
                context.report.params,
                connectionUuid,
                {
                    'Uuid': extsyncdocUuid,
                    'Direction': direction
                },
                objects
            );
            return true;
        } catch (err) {
            this.set_variable(context, '_last_error', err)
            return false;
        }
    }

    async prepare_ext_sync_objects(context, direction, objects) {
        for (const obj of objects) {
            obj.Action = direction
        }
        return objects
    }

    async prepare_api3_link(context, direction, objects) {
        const _objects = []
        for (const obj of objects) {
            const _id = obj.ИдИС
            const iniName = obj.ini_name
            const _object = {
                Id: _id,
                Action: direction,
                Title: obj.Название,
                Type: obj.ТипИС ? `${obj.ТипИС}.${obj.ИмяИС}` : obj.ИмяИС,
                ClientId: _id,
                Data: {
                    ini_name: iniName,
                    _print_forms: obj._print_forms
                },
                StatusId: null
            }
            _objects.push(_object)
        }
        return _objects
    }

    async prepare_api3_objects(context, direction, objects) {
        const _objects = []
        for (const obj of objects) {
            const _id = obj.ИдИС
            const iniName = obj.ini_name
            const _object = {
                Id: _id,
                Action: direction,
                Title: obj.Название,
                Type: obj.ТипИС ? `${obj.ТипИС}.${obj.ИмяИС}` : obj.ИмяИС,
                ClientId: _id,
                Data: {
                    ini_name: iniName,
                    data_is: obj,
                    _print_forms: obj._print_forms
                },
                StatusId: 'Получен'
            }
            _objects.push(_object)
        }
        return _objects
    }
}
