import {ServiceException, ErrorInBlock} from "@/BlocklyExecutor/Core/exceptions"
import {SimpleBlock} from "@/BlocklyExecutor/Core/block_templates/simple_block"
import {objHasOwnProperty} from "@/Helpers/BaseHelper"
import {UserError} from "@/Helpers/ExtException";

export default class ExecuteWorkspace extends SimpleBlock {
    async _calc_value(node, path, context, block_context) {
        try {
            let params_count = Number(this.workspace.find_mutation_by_name(node, 'param_count', 0))
            let workspace_name = block_context.NAME
            let endpoint = block_context.ENDPOINT
            if (!objHasOwnProperty(block_context, '_previous_workspace')) {
                block_context['_previous_workspace'] = this.executor.workspace.name
            }
            if (!objHasOwnProperty(block_context, '_child')) {
                block_context['_child'] = {'variable_scopes': [{}]}
                for (let i = 0; i <= params_count; i++) {
                    const key = block_context[`PARAM_NAME_${i}`]
                    const value = block_context[`PARAM_VALUE_${i}`]
                    block_context['_child']['variable_scopes'][0][key] = value
                }
            }

            const nested_context = context.init_nested(block_context, workspace_name)
            const result = await this.executor.execute_nested(
                nested_context,
                endpoint,
                this.executor.commands_result
            )
            this.executor.workspace.name = block_context._previous_workspace
            context.current_workspace = this.executor.workspace.name
            context.set_next_step()
            return result
        } catch (err) {
            if (err instanceof ServiceException) {
                throw err
            }
            if (err instanceof UserError) {
                this.executor.workspace.name = block_context._previous_workspace
            }
            switch (err.constructor.name) {
                case 'ExtException':
                    throw err
                default:
                    throw new ErrorInBlock({parent: err})
            }
        }
    }
}
