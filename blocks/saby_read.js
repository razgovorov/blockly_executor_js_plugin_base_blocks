import {SimpleBlock} from "@/BlocklyExecutor/Core/block_templates/simple_block";

export default class SabyRead extends SimpleBlock {
    async _calc_value(node, path, context, block_context) {
        const _type = block_context.type
        let _id = block_context.id
        let single = false

        if (typeof _id === 'string') {
            _id = [_id]
            single = true
        }

        const result = await context.report.service.api3GetSbisObjects(context.report.params, _type, _id)
        if (single) {
            return result[0]
        }
        return result
    }
}
