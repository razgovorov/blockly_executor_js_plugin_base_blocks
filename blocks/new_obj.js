import Block from "@/BlocklyExecutor/Core/Block"
import {objHasOwnProperty} from "@/Helpers/BaseHelper"

export default class NewObj extends Block {
    async _execute(node, path, context, block_context) {
        let mutation_count = Number(this.workspace.find_mutation_by_name(node, 'PROP', 0))
        if (!objHasOwnProperty(block_context, this._result)) {
            block_context[this._result] = {}
        }
        if (mutation_count) {
            for (let j = 0; j < mutation_count; j++) {
                // рассчитываем все мутации
                let prop_name = this.workspace.find_field_by_name(node, `PROP${j}_NAME`)
                if (!prop_name || objHasOwnProperty(block_context[this._result], prop_name)) {
                    continue
                }
                let node_prop_value = this.workspace.find_input_by_name(node, `PROP${j}_VALUE`)
                block_context[this._result][prop_name] = await this.execute_all_next(node_prop_value,
                    `${path}.PROP${j}_VALUE`, context, block_context)
            }
            this._check_step(context, block_context)
            return block_context[this._result]
        }
    }
}
