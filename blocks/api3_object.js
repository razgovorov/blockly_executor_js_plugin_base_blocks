import {SimpleBlock} from "@/BlocklyExecutor/Core/block_templates/simple_block";
import {isObject} from "@/Helpers/BaseHelper";

export default class Api3Object extends SimpleBlock {
    obj_name = ''
    obj_type = ''

    async _calc_value(node, path, context, block_context) {
        const data = isObject(block_context.INIT_VALUE) ? block_context.INIT_VALUE : {}
        if (this.obj_type) {
            data.ТипСБИС = this.obj_type
        }
        if (this.obj_name) {
            data.ИмяСБИС = this.obj_name
        }
        for (let elem in block_context) {
            if (elem[0] !== '_' && block_context[elem] !== null) {
                data[elem] = block_context[elem];
            }
        }
        return data;
    }
}
