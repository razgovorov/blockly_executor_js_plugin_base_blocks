import {SimpleBlock} from "@/BlocklyExecutor/Core/block_templates/simple_block"


export default class ObjPropSet extends SimpleBlock {
    async _calc_value(node, path, context, block_context) {
        let variable_name = block_context['VAR'] || ''
        let obj = this.get_variable(context, variable_name)
        obj[block_context['PATH'] || ''] = block_context['VALUE' || '']
        this.set_variable(context, variable_name, obj)
    }
}
