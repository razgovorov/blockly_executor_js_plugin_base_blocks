import {SimpleBlock} from "@/BlocklyExecutor/Core/block_templates/simple_block";

export default class ObjActionListViewIconColumn extends SimpleBlock {
    async _calc_value(node, path, context, block_context) {
        const param = {}
        this.copyBlockContext(param, block_context)
        param.ColumnType = 'Icon'
        return param
    }
}
