import extsyncdoc_write from './extsyncdoc_write'
import api3_link from './api3_link'
import api3_object from './api3_object'
import compare_objects from './compare_objects'
import controls_forEach2 from './controls_forEach2'
import execute_workspace from './execute_workspace'
import format from './format'
import get_current_datetime from './get_current_datetime'
import new_obj  from './new_obj'
import obj_prop_append from './obj_prop_append'
import obj_prop_get from './obj_prop_get'
import obj_prop_set from './obj_prop_set'
import re_search from './re_search'
import split from './split'
import test_equal from './test_equal'
import extsyncdoc_run from './extsyncdoc_run'
import saby_extsyncobj_list from './saby_extsyncobj_list'
import saby_download_from_link from './saby_download_from_link'
import extsys_calc_ini from './extsys_calc_ini'
import extsys_update_status2 from './extsys_update_status2'
import saby_read_document from './saby_read_document'
import saby_read from './saby_read'
import saby_find from './saby_find'
import invite_user from './invite_user'
import saby_execute_action from './saby_execute_action'
import accordion_item from './accordion_item'
import accordion_item2 from './accordion_item2'
import obj_action_list_view from './obj_action_list_view'
import obj_action_list_view_filter from './obj_action_list_view_filter'
import obj_action_list_view_column from './obj_action_list_view_column'
import obj_action_list_view_toolbar from './obj_action_list_view_toolbar'
import obj_action_list_view_toolbar_search from './obj_action_list_view_toolbar_search'

// TODO удалить поддержку, после переезда всех инишек
import obj_action_flat_list_view from './deprecated/obj_action_flat_list_view'
import obj_action_master_detal_view from './deprecated/obj_action_master_detal_view'
import obj_action_list_view_document_column from './deprecated/obj_action_list_view_document_column'
import obj_action_list_view_date_number_column from './deprecated/obj_action_list_view_date_number_column'
import obj_action_list_view_icon_column from './deprecated/obj_action_list_view_icon_column'
import obj_action_tasks_list_view from './deprecated/obj_action_tasks_list_view'
import obj_page_filter_chips from './deprecated/obj_page_filter_chips'
import obj_action_field_connection_view_filter from './deprecated/obj_action_field_connection_view_filter'
import obj_action_field_enumeration_view_filter from './deprecated/obj_action_field_enumeration_view_filter'
import obj_action_field_enumeration_source_filter from './deprecated/obj_action_field_enumeration_source_filter'
import obj_action_input_string_view_filter from './deprecated/obj_action_input_string_view_filter'
import obj_action_input_number_view_filter from './deprecated/obj_action_input_number_view_filter'
import obj_action_date_view_filter from './deprecated/obj_action_date_view_filter'
import obj_action_hierarchical_list_view from './deprecated/obj_action_hierarchical_list_view'
import obj_action_list_view_employee_column from './deprecated/obj_action_list_view_employee_column'
import obj_action_enumeration_with_date_view_filter from './deprecated/obj_action_enumeration_with_date_view_filter'

import view_template_flat from './view_template_flat'
import view_template_masterdetal from './view_template_masterdetal'
import view_template_hierarchical from './view_template_hierarchical'
import view_template_tasks from './view_template_tasks'

import view_column_document from './view_column_document'
import view_column_default from './view_column_default'
import view_column_date_number from './view_column_date_number'
import view_column_icon from './view_column_icon'
import view_column_employee from './view_column_employee'

import view_filter_chips from './view_filter_chips'
import view_filter_link from './view_filter_link'
import view_filter_enumeration from './view_filter_enumeration'
import view_filter_string from './view_filter_string'
import view_filter_number from './view_filter_number'
import view_filter_period from './view_filter_period'
import view_filter_enumeration_with_date from './view_filter_enumeration_with_date'

import view_source from './view_source'

export default {
    blocks: {
        extsyncdoc_write,
        api3_link,
        api3_object,
        compare_objects,
        controls_forEach2,
        execute_workspace,
        format,
        get_current_datetime,
        new_obj,
        obj_prop_append,
        obj_prop_get,
        obj_prop_set,
        re_search,
        split,
        test_equal,
        extsyncdoc_run,
        saby_extsyncobj_list,
        saby_download_from_link,
        extsys_calc_ini,
        extsys_update_status2,
        saby_read_document,
        saby_read,
        saby_find,
        invite_user,
        saby_execute_action,
        accordion_item,
        accordion_item2,
        obj_action_list_view,
        obj_action_list_view_filter,
        obj_action_list_view_column,
        obj_action_list_view_toolbar,
        obj_action_list_view_toolbar_search,
        obj_action_flat_list_view,
        obj_action_master_detal_view,
        obj_action_list_view_document_column,
        obj_action_list_view_date_number_column,
        obj_action_list_view_icon_column,
        obj_action_tasks_list_view,
        obj_page_filter_chips,
        obj_action_field_connection_view_filter,
        obj_action_field_enumeration_view_filter,
        obj_action_field_enumeration_source_filter,
        obj_action_input_string_view_filter,
        obj_action_input_number_view_filter,
        obj_action_date_view_filter,
        obj_action_hierarchical_list_view,
        obj_action_list_view_employee_column,
        obj_action_enumeration_with_date_view_filter,

        view_template_flat,
        view_template_masterdetal,
        view_column_document,
        view_column_default,
        view_column_date_number,
        view_column_icon,
        view_template_tasks,
        view_filter_chips,
        view_filter_link,
        view_filter_enumeration,
        view_source,
        view_filter_string,
        view_filter_number,
        view_filter_period,
        view_template_hierarchical,
        view_column_employee,
        view_filter_enumeration_with_date,
    }
}
