import {SimpleBlock} from "@/BlocklyExecutor/Core/block_templates/simple_block";
import {isObject} from "@/Helpers/BaseHelper";

export default class SabyReadDocument extends SimpleBlock {
    async _calc_value(node, path, context, block_context) {
        try {
            const doc = block_context.doc
            let docUuid = doc.Идентификатор
            if (isObject(doc)) {
                docUuid = doc
            } else if (typeof doc === 'string') {
                docUuid = doc
            } else {
                throw new Error('not supported document type')
            }
            return await context.report.service.readDocument(context.report.params, {Идентификатор: docUuid})
        } catch (err) {
            if (err instanceof TypeError) {
                throw new Error(`${this.constructor.name} ${this.block_id} params not defined`)
            }
        }
    }
}
