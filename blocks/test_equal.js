import {SimpleBlock} from "@/BlocklyExecutor/Core/block_templates/simple_block"
import ExtException from "@/Helpers/ExtException"

export default class TestEqual extends SimpleBlock {
    async _calc_value(node, path, context, block_context) {
        if (block_context['ACTUAL_VALUE'] !== block_context['DESIRED_VALUE']) {
            throw new ExtException({
                message: 'Test failed',
                detail: `${block_context['NAME']} = ${block_context['ACTUAL_VALUE']} должно быть ${block_context['DESIRED_VALUE']}`
            })
        }
    }
}
