import MultiThreadLoop from "@/BlocklyExecutor/Core/block_templates/MultiThreadLoop";

export default class SabyExtsyncobjList extends MultiThreadLoop {
    async _getItems(context, block_context) {
        const extsyncdocUuid = context.report.uuid
        const extraFields = []

        const filter = {}

        const sorting = []
        const pagination = {
            PageSize: 50,
            Page: block_context.page || 0
        }

        const result = await context.report.service.extsyncobjList(
            context.report.params,
            extsyncdocUuid,
            extraFields,
            filter,
            sorting,
            pagination
        )
        return result.Result || []
    }
}
