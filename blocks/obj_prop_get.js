import {SimpleBlock} from "@/BlocklyExecutor/Core/block_templates/simple_block"
import Helper from "@/Helpers/Helper";


export default class ObjPropGet extends SimpleBlock {
    async _calc_value(node, path, context, block_context) {
        let obj_name = block_context['VAR']
        path = block_context['PATH']
        let obj = this.get_variable(context, obj_name)
        if (path && path[0] === "\"") {
            path = path.slice(1, path.length)
            if (path[-1] === "\"") {
                path = path.slice(0, path.length - 1)
            }
            return obj.get(path)
        } else {
            return Helper.objGetPathValue(obj, path, '.')
        }
    }
}
