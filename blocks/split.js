import {SimpleBlock} from "@/BlocklyExecutor/Core/block_templates/simple_block"


export default class Split extends SimpleBlock {

    async _calc_value(node, path, context, block_context) {
        return block_context['value'].split(block_context['delimiter'])
    }
}
