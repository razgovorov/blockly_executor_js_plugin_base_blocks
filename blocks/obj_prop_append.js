import {SimpleBlock} from "@/BlocklyExecutor/Core/block_templates/simple_block"


export default class ObjPropAppend extends SimpleBlock{
    async _calc_value(node, path, context, block_context){
        let obj = this.get_variable(context, block_context['VAR'])
        if(block_context['PATH']) {
            obj[block_context['PATH']].append(block_context['VALUE'])
        }
        else {
            obj.append(block_context['VALUE'])
        }
        this.set_variable(context, block_context['VAR'], obj)  // todo для объектов не заменять объект, а обновлять свойство
    }
}

