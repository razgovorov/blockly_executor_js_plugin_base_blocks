import {SimpleBlock} from "@/BlocklyExecutor/Core/block_templates/simple_block";

export default class ExtsysUpdateStatus2 extends SimpleBlock {
    async _calc_value(node, path, context, block_context) {
        const service = getServiceByUrl()
        const account = await context.report.service.externalIDCurrentClient()

        const docStatus = [{
            ИмяСБИС: '',
            ИдИС: block_context.ИдИС,
            ИмяИС: block_context.ИмяИС,
            АктивныйЭтап: block_context.ACTIVE_STAGE || '',
            КодСостоянияЭДО: block_context.STATE_CODE,
            ВнешнийИдСБИС: block_context.SbisID,
            Аккаунт: account,
            Сервис: service
        }];

        await context.report.connectorIs.loadCalcIni('UpdateStatus', docStatus)
    }
}

function getServiceByUrl() {
    const regex = /https:\/\/(.*?)-ie-.+\.saby\.ru/gmi;
    const match = regex.exec(location.href);
    return match ? match[1] : '';
}
