import {SimpleBlock} from "@/BlocklyExecutor/Core/block_templates/simple_block";
import {isObject} from "@/Helpers/BaseHelper";

export default class SabyExecuteAction extends SimpleBlock {
    async _calc_value(node, path, context, block_context) {
        const checkResult = this.checkProps(node, path, context, block_context)
        if (!checkResult) {
            return false
        }
        const doc = block_context.DOCUMENT
        const easySend = this._get_mutation(node, 'EASY_SEND')
        const actionName = easySend === 'TRUE' ? 'ПростоОтправить' : block_context.ACTION
        const comment = block_context.COMMENT || ''
        const stage = Array.isArray(doc.Этап) ? doc.Этап[0] : doc.Этап
        stage.Действие = {
            Название: actionName,
            Комментарий: comment
        }
        if (easySend === 'TRUE') {
            stage.Действие.Название = 'ПростоОтправить'
            stage.Название = 'ПростаяОтправка'
        }
        const attachments = this.getAttachment(node, path, context, block_context)
        if (attachments.length) {
            const doc1 = {
                Идентификатор: doc.Идентификатор,
                Этап: stage,
                Вложение: attachments
            }
            try {
                await context.report.service.writeAttachment(context.report.params, doc1)
            } catch (err) {
                this.set_variable(context, '_last_error', `СБИС.ЗаписатьВложение ${err}`)
                this.logger.error(`СБИС.ЗаписатьВложение ${err}`)
                this.logger.error(JSON.stringify(doc1))
                return false
            }
        }
        const doc1 = {
            Идентификатор: doc.Идентификатор,
            Этап: stage,
        }
        let prepareResult
        try {
            prepareResult = await context.report.service.prepareAction(context.report.params, doc1)
        } catch (err) {
            context.variables._last_error = err.toString()
            this.set_variable(context, '_last_error', `СБИС.ПодготовитьДействие ${err}`)
            this.logger.error(`СБИС.ПодготовитьДействие ${err}`)
            this.logger.error(JSON.stringify(doc1))
            return false
        }

        let signArray
        try {
            const preparedStage = prepareResult.Этап
            const executeAction = {'Идентификатор': prepareResult.Идентификатор}
            if (!preparedStage) {
                throw new Error(`${this.constructor.name} ${this.block_id} Failed to prepare the document stage`)
            }
            if (Array.isArray(preparedStage)) {
                executeAction.Этап = preparedStage[0]
                // eslint-disable-next-line no-empty
                if (signArray) {}
            }
            return await context.report.service.executeAction(context.report.params, executeAction)
        } catch (err) {
            context.variables._last_error = err.toString()
            this.set_variable(context, '_last_error', `СБИС.ВыполнитьДействие ${err}`)
            this.logger.error(`СБИС.ВыполнитьДействие ${err}`)
            this.logger.error(JSON.stringify(prepareResult))
            return false
        }
    }

    checkProps(node, path, context, block_context) {
        const doc = block_context.DOCUMENT
        const easySend = this._get_mutation(node, 'EASY_SEND')
        const actionName = easySend === 'TRUE' ? 'ПростоОтправить' : block_context.ACTION
        if (!doc || !isObject(doc)) {
            context.variables._last_error = `${this.constructor.name} ${this.block_id} Param Document defined incorrectly`
            return false
        }
        if (!actionName || !(typeof actionName === 'string')) {
            context.variables._last_error = `${this.constructor.name} ${this.block_id} Param Action name incorrectly`
            return false
        }
        if (!doc.Идентификатор || !(typeof doc.Идентификатор === 'string')) {
            context.variables._last_error = `${this.constructor.name} ${this.block_id} Param Document defined incorrectly`
            return false
        }
        const stage = doc.Этап
        if (!Array.isArray(stage)) {
            context.variables._last_error = `${this.constructor.name} ${this.block_id} Param Document defined incorrectly`
            return false
        }
        if (!stage) {
            context.variables._last_error = `${this.constructor.name} ${this.block_id} Document dont have specified stage`
            return false
        }
        return true
    }

    getAttachment(node, path, context, block_context) {
        let attachments = []
        let attachmentTypes = this._get_mutation(node, 'attachment_types')
        if (attachmentTypes) {
            attachmentTypes = attachmentTypes.split(',')
        } else {
            return attachments
        }
        for (const [attIndex, attType] of attachmentTypes.entries()) {
            if (attType === 'att_array') {
                let attachmentArray
                try {
                    attachmentArray = block_context[`ATT${attIndex}`]
                    if (!attachmentArray) {
                        throw new TypeError()
                    }
                } catch (err) {
                    if (err instanceof TypeError) {
                        throw new Error(`${this.constructor.name} не указаны данные вложения`)
                    }
                }
                attachments = [...attachments, attachmentArray]
            } else if (attType === 'att_b64') {
                let attachmentData
                try {
                    attachmentData = block_context[`ATT${attIndex}_DATA`]
                } catch (err) {
                    if (err instanceof TypeError) {
                        throw new Error(`${this.constructor.name} не указаны данные вложения`)
                    }
                }
                const attachmentTitle = block_context.get(`ATT${attIndex}_TITLE`)
                const attachment = {
                    Файл: {
                        Имя: attachmentTitle,
                        ДвоичныеДанные: attachmentData
                    }
                }
                attachments.push(attachment)
            } else {
                throw new Error(`${this.constructor.name} неподдерживаемы тип вложения ${attType}`)
            }
        }
        return attachments
    }
}
