import {SimpleBlock} from "@/BlocklyExecutor/Core/block_templates/simple_block";

export default class SabyFind extends SimpleBlock {
    required_param = ['type', 'object']

    async _calc_value(node, path, context, block_context) {
        const objType = block_context.type
        const objFilter = block_context.object
        return await context.report.service.FindSbisObject(objType, objFilter)
    }
}
