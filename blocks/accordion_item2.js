import {SimpleBlock} from "@/BlocklyExecutor/Core/block_templates/simple_block";

export default class AccordionItem2 extends SimpleBlock {
    async _calc_value(node, path, context, block_context) {
        const param = {}
        this.copyBlockContext(param, block_context)
        switch (block_context['_folder']){
            case 'elem':
                param['parent@'] = false
                param['action'] = true
                break
            case 'group':
                param['parent@'] = true
                param['action'] = false
                break
            case 'folder':
                param['parent@'] = true
                param['action'] = true
                break
        }
        return param
    }
}
