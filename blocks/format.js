import {SimpleBlock} from "@/BlocklyExecutor/Core/block_templates/simple_block"
// from datetime import datetime
import {objHasOwnProperty} from "@/Helpers/BaseHelper"


// eslint-disable-next-line no-unused-vars
function format_to_date(value, format_string) {
    // let date = datetime.strptime(value, format_string)
    // return date.strftime('%Y-%m-%dT%H:%M:%S.%')
}

// eslint-disable-next-line no-unused-vars
function format_from_date(value, format_string) {
    // let date = datetime.strptime(value, '%Y-%m-%dT%H:%M:%S.%')
    // return date.strftime(format_string)
}

// eslint-disable-next-line no-unused-vars
function format_to_string(value, format_string) {
    return String(value)
}

// eslint-disable-next-line no-unused-vars
function format_to_number(value, format_string) {
    try {
        return Number(value)
    } catch(err) {
        return 0
    }
}

const operations = {
    'to_date': format_to_date,
    'from_date': format_from_date,
    'to_number': format_to_number,
    'to_string': format_to_string
}


export default class Format extends SimpleBlock {
    async _calc_value(node, path, context, block_context) {
        if (!objHasOwnProperty(operations, block_context['type'])) {
            throw new Error(`NotImplemented block ${this.__class__.__name__} operation ${block_context["type"]} `)
        }
        return operations[block_context['type']](block_context['value'], block_context['template'])
    }
}
