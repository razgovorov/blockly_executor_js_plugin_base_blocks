import {SimpleBlock} from "@/BlocklyExecutor/Core/block_templates/simple_block";

export default class AccordionItem extends SimpleBlock {
    async _calc_value(node, path, context, block_context) {
        const result = []
        const param = {}
        this.copyBlockContext(param, block_context)
        param.level = 1
        const children = block_context.children
        if (children) {
            param.column = 1
            param['parent@'] = true
            this.fillChildren(children, param.id, result)
            delete param.children
        } else {
            param['parent@'] = false
        }
        result.push(param)
        return result
    }

    fillChildren(children, parent, result) {
        for (const childs of children) {
            for (const child of childs) {
                child.level = child.level + 1
                child.column = 1
                child.parent = parent
                result.push(child)
            }
        }
    }
}
