import {DeferredOperation, LimitCommand, ReturnFromFunction, StepForward} from "@/BlocklyExecutor/Core/exceptions";
import ExtsysConnectorApi, {updateIni} from "@/BlocklyExecutor/Core/block_templates/extsys_connector_api";
import ExtException from "@/Helpers/ExtException";
import Block from "@/BlocklyExecutor/Core/Block";

export default class ExtsysCalcIni extends ExtsysConnectorApi {
    async _calc_mutation(node, path, context, block_context, mutation_number) {
        const nodeKey = node.querySelector(`field[\\@name='KEY${mutation_number}']`)
        const key = nodeKey.textContent
        const nodeValue = node.querySelector(`value[\\@name='VALUE${mutation_number}']`)
        if (key && nodeValue) {
            const result = await this.execute_all_next(nodeValue, `${path}.param${mutation_number}`, context, block_context)
            return {
                name: key,
                value: result
            }
        }
        return null
    }

    async _calc_value(node, path, context, block_context) {
        if (Block.commandSended(block_context)) {
            try {
                return this.commandGetResult(block_context.__deferred)
            } catch (err) {
                throw ExtException({
                    parent:err,
                    dump: {
                        'block_context': block_context,
                        'block_id': this.block_id,
                        'full_name': this.full_name
                    }
                })
            }
        }
        try {
            const paramsCount = Number(this._get_mutation(node, 'items', 0))
            const iniName = block_context['INI_NAME']
            const params = {
                'ini_name': iniName,
            }
            for (let i = 0; i < paramsCount; i++) {
                const key = block_context[`PARAM${i}_NAME`]
                params[key] = block_context[`PARAM${i}_VALUE`]
            }
            await updateIni(this.executor, context, iniName)
            await this.commandSend('calc_ini', params, context, block_context)
        } catch (err) {
            if (err instanceof DeferredOperation) {
                context.add_deferred(err)
                throw err
            }
            if (
                err instanceof LimitCommand ||
                err instanceof StepForward ||
                err instanceof ReturnFromFunction
            ) {
                throw err
            }
            throw new ExtException({
                parent: err,
                action: `${this.constructor.name}`
            })
        }
    }
}
